#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define MAX_NAME_LENGTH             256

int     DEVICE_connect_port( SERIAL_DEVICE *dev );
void    DEVICE_set_params( SERIAL_DEVICE dev );
void    DEVICE_set_baudrate( SERIAL_DEVICE dev, int baudrate );


