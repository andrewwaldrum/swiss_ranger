/*New Awesimo.ccp class implementation.


*/
#include <math.h>
#include <iostream>
//#include "serial-io.h"   //Talks to serial servo conttroller
//#include "serial.h"
#include "Awesimo.h"  //My values and functions

Awesimo::Awesimo() 
{

//attributes:variables

/***   Relevant constants   ***/
/*#define CENTERED 50
#define  HARD_RIGHT 100
#define  HARD_LEFT  0
#define  BRAKE  48
#define  SLOW  52
#define  REVERSE  46
#define NEUTRAL 50
*/

  // File descriptor to the Swiss Ranger
/*  int fd;

// Counter
int i;

int throttle, steering;//these values are the current servo settings
//int t_incriment;
int *steering_ptr,*throttle_ptr;

int s_incriment;//the steering incriment 

//int throttle_g;//These are the goal throttle and steering settings 
int throttle_subslow;//new modifier
int throttle_counter;// counts steps in 
*/

//Methods:functions
void startup();
void setSerialDevice(SERIAL_DEVICE *dev);
void startSerialDevice( SERIAL_DEVICE *dev );
int write_minissc_command(int fd, unsigned char motor, unsigned char position) ;
int steering_set(int fd, int servoValue);
int head_set(int fd, int servoValue);

int throttle_set(int fd, int servoValue);
unsigned char getSteeringValue(double valDouble);


};

/*Set initial realtionships, startup stuff, etc
 */
void Awesimo::startup(void)
{ 

 // image_ptr=image;//points the pointer to the pixels.  duh.  
  //range_ptr=range;//points the pointer to the pixels.  duh.  
  //pix_pointer=pixels;//points the pointer to the pixels.  duh.  
  //calibration_ptr= calibration;
//  image_ptr=range_ptr+SWISSRANGER_ROWS*SWISSRANGER_COLUMNS*2;
  running_time=60;//time in seconds for sim to run
  throttle_counter = 0;//initialize the subslow counter
  s_incriment = 15;// Maximum change in steering per step
steering_ptr=&steering;
throttle_ptr= &throttle;

  startSerialDevice(&dev);
  throttle = BRAKE;
  steering = CENTERED;
  steering_set(dev.fd, steering);   //straighten out steering
  head_set(dev.fd,21); //put truck in low gear--unused on Awesimo
  throttle_set(dev.fd, throttle);   //Brakes

  running_time=running_time*10;//convert to 1/10 of a second
  i = 1;
}

void Awesimo::setSerialDevice(SERIAL_DEVICE *dev)
{
  dev->baud           = DEFAULT_SERIAL_BAUD;
  dev->parity         = DEFAULT_SERIAL_PARITY;
  dev->fd             = -1;
  dev->databits       = DEFAULT_SERIAL_DATA_BITS;
  dev->stopbits       = DEFAULT_SERIAL_STOP_BITS;
  dev->hwf            = DEFAULT_SERIAL_HWF;
  dev->swf            = DEFAULT_SERIAL_SWF;
  dev->ttyport        =
    (char *) malloc( (strlen(DEFAULT_SERIAL_PORT)+1)*sizeof(char) );
  strcpy( dev->ttyport, DEFAULT_SERIAL_PORT );
}

/*
 *  Function to set up serial ports, called in main
 */
void Awesimo::startSerialDevice( SERIAL_DEVICE *dev )
{
  setSerialDevice( dev );
  fprintf( stderr, "INFO: connect TTY %-14s ...... ", dev->ttyport );
  DEVICE_connect_port( dev );
  if( dev->fd == -1) {
    fprintf( stderr, "failed\n" );
    exit(1);
  }
  fprintf( stderr, "ok\n" );
  fprintf( stderr, "INFO: set port param %6d:%d%c%d ....... ",
           dev->baud,
           dev->databits,
           ( dev->parity==N ?
             'N' : dev->parity==E ? 'E' : 'O' ),
           dev->stopbits );
  DEVICE_set_params( *dev );
  fprintf( stderr, "ok\n" );
}


/*
 *  Writes command to Mini SSC, protocol consists of three bytes:
 *  1) 255 synch byte, 2) servo number, 3)servo value
 */
int Awesimo::write_minissc_command(int fd, unsigned char motor, unsigned char position) 
{
  unsigned char command[5];

  command[0] = 255;
  command[1] = motor;
  command[2] = position;
  if (motor==1)
    fprintf(stderr, "Steering set %d\n", position);
  else if (motor==2)
    fprintf(stderr, "Throttle set %d\n", position);
  else
    fprintf(stderr, "GEAR     set %d\n", position);
  writeData(fd, command, 3);
 // sleep(SLEEPDELAY);
return 1;
}

unsigned char Awesimo::getSteeringValue(double valDouble)
{
return valDouble;
}


int Awesimo::steering_set(int fd, int servoValue)
{	
	int val;
	val=getSteeringValue(SERVO1_MIN + servoValue / 100.0 * (SERVO1_MAX - SERVO1_MIN));
  //  servoValue = 100 - servoValue;//used to reverse the servo
  write_minissc_command(fd,SERVO1_ID,val );
  return 1;
}

/*Sets the gear ratio for the throttle.  This should almost always be 15*/
/* ex:    gear_ratio_set(dev.fd, 15);   //put truck in low gear */
int Awesimo::head_set(int fd, int servoValue)
{                                              
  write_minissc_command(fd, SERVO3_ID, SERVO3_MIN +
                        servoValue / 100.0 * (SERVO3_MAX - SERVO3_MIN));
  return 1;
}


/*Sets throttle level */
//  throttle_set(dev.fd, 53);
// as of 11/1/05 these are some values and expectet actions: 
   // 52 --SLOW  forward
   // 50 --neutral
   // 48 --brake
   // 45 -backwards
   // try higher values with caution */
int Awesimo::throttle_set(int fd, int servoValue)
{
	int val;
	val=getSteeringValue(SERVO1_MIN + servoValue / 100.0 * (SERVO1_MAX - SERVO1_MIN));
  //  servoValue = 100 - servoValue;//used to reverse the servo
 	 write_minissc_command(fd, SERVO2_ID, val);
  return 1;
}
