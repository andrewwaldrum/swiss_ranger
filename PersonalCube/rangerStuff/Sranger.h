#ifndef SRANGER_H
#define SRANGER_H

#ifdef __cplusplus
extern "C" {
#endif


//#include <qgl.h>
#include "swissranger.h" //Ranger Library

class SRanger// : public QGLWidget
{
public:
    //SRanger(*parent = 0, const char *name = 0);
	void load_data(void);
	/*double featureRange;
	double featureBearingX;
	double featureBearingY;
	double featureStatus;
*/
	int noiseFilter;
	double featureBearingX;
	double featureBearingY;
	unsigned short featureRange;
	double featureRangeBest;   
	unsigned short pixels[19840*2];
 	void MytimerEvent( void );
	int closeRanger(void);
	


protected:

   // void MytimerEvent( QTimerEvent * );

	short unsigned int pinchNum(short unsigned int number);
	
	double wuzzyFix(unsigned short best, double bestStatus, unsigned short newData, double newDataStatus);

	int filter;
	double viewer_offset;

private:
 
	int saturationCheck(int k);
	void trackSaturationFeature(int k);

	//int len 19840;
	int fd;
	int res;
	int sample_size;
	int normalTimer;
	
};


#ifdef __cplusplus
}
#endif
#endif

