#ifndef SERIAL_H
#define SERIAL_H

int cmrt_serial_connect(int *dev_fd, char *dev_name);

void cmrt_serial_configure(int dev_fd, int baudrate, char *parity);

long cmrt_serial_numChars(int dev_fd);

int cmrt_serial_ClearInputBuffer(int dev_fd);

int cmrt_serial_writen(int dev_fd, char *buf, int nChars);

int cmrt_serial_readn(int dev_fd, char *buf, int nChars);

#endif
