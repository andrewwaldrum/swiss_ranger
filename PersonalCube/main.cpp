#include <qapplication.h>
#include "srGui.h"

int main( int argc, char ** argv )
{
    QApplication a( argc, argv );
    spring01 w;
    w.show();
    a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
    return a.exec();
}
