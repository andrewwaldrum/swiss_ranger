#ifndef WDECIDER_H
#define WDECIDER_H

//#include <actions.h>
/*
#define SLOW 44
#define BRAKE 33
#define  SLOW_REVERSE 28
#define CENTERED 50
#define LEFT 0
#define RIGHT 100
*/
#define ONE -125
#define TWO -124
#define THREE -123
#define FOUR 1
#define FIVE 125
#define SIX 124
#define SEVEN 123
#define EIGHT -1
#define LOOK_AHEAD 50

#define LOOK_RIGHT 38
#define LOOK_FAR_RIGHT 26
#define LOOK_VERY_FAR_RIGHT 14

#define LOOK_LEFT 62
#define LOOK_FAR_LEFT 74
#define LOOK_VERY_FAR_LEFT 86

class wDecider
{
public:
	wDecider(void);
	void decide(void);
	void decideTrack(void);
 	void decideHead(void); 
	void decideHeadPID(void);
	void decideOA(void);
	int pastHead;
	int headDif;
	int selector;
	int steeringSetting;
	int throttleSetting;
	int headSetting;
	int behavior;
	int pastBehavior;
	int featureY;
	int featureX;
	int featureRange;
	unsigned short *pixPtr;
	unsigned short pixels[19840*2];
	double featureBearingX;
	double featureBearingY;
	unsigned short featureRangeBest;
	unsigned short featureRangeBestPast;
	int rangeDelta;
	double featureStatus;
	int noiseFilter;
	int yellow[16];
	int red[16];
	int featureGoalRange;
	int featureTolerance;
	int deltaTolerance;

protected:

	short unsigned int pinchNum(short unsigned int number);
	void featureSearch();
	//double wuzzyFix(unsigned short best, double bestStatus, unsigned short newData);
	double wuzzyFix(unsigned short best, double bestStatus, unsigned short newData, double newDataStatus);
	void trackSaturationFeature(int k);
	void obstacleSearchOA(void);	
	void headOA();
	double headRange2AwesimoRange(void);
	int steerFix(int);
	int FixHead2BodyOAField(void);
	int OAFieldEval(int field, int direction);
	void OaFieldCompare(int proxField[16]);
	void initializeRangeLimits(void);
	void decideTrackFeature(void);
	void SetThrottle(int dir);
//Variables
	
	int headOAtell;
	int PastSteeringSetting;
	int PastThrottleSetting;	
	int PastHeadSetting;	

	int filter;
	double viewer_offset;

//private slots:
	int getfeatureBearingY(void);
	int saturationCheck(int k);
	int RangeCorrection(int rawRange);

private:


};


#endif

 
