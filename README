SWISS RANGER 2 LINUX DEVICE DRIVER

This is the Linux, user-side driver for CSEM's Swiss Ranger 2.

Version: 15.02.2005
Author: Gabriel Gruener, Gabriel.Gruener@csem.ch

Copyright � 2004-2005, CSEM
All rights reserved.
Proprietary software. Use is subject to license terms as published in:

        http://www.csem.ch/fs/copyright.htm



REQUIREMENTS

This driver has been tested under Linux 2.4.26 and 2.6.10. It
requires libusb-0.1.8, which itself requires usbdevfs. The Swiss
Ranger device file must be readable and writable. If you want to
access the Swiss Ranger as a user other than root, make sure you give
the appropriate permissions with chmod. For a permanent solution,
modify the usbdevfs entry in /etc/fstab by setting devmode
appropriately, for example:

usbdevfs  /proc/bus/usb  usbdevfs  devmode=0666,noauto 0 0

For other mount options, refer to the usbdevfs implementation.

Note that in some distros the USB file system might not be declared
inside /etc/fstab, but rather directly mounted during initialization
(e.g. in /etc/rc.sysinit).


FILES INCLUDED IN THIS PACKAGE

README                 This file
Makefile               Makefile for the driver
swissranger.h          Swiss Ranger include file
swissrager.c           The Swiss Ranger driver
swissrangerTester.c    Demonstrates how the driver is used to talk to
                       the camera


BUILDING

To compile the Swiss Ranger driver do:

> tar xvjf swissranger-1.3.tar.bz2
> cd swissranger
> make

This will create the shared library libswissranger.so. You may place
this library in your preferred location (e.g. /usr/local/lib). If you
run into trouble, uncomment the SWISSRANGER_DEBUG flag in the
Makefile and recompile. This will make the driver print a rich
description of its actions to stderr.

To compile the swissrangerTester on the swissranger directory do:

> gcc -L . -lswissranger -Wall -O3 -o swissrangerTester swissrangerTester.c



$Id: README,v 1.5 2005/02/15 18:46:35 ggr Exp $
