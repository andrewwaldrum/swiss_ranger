#ifndef RANGERGL_H
#define RANGERGL_H

#ifdef __cplusplus
extern "C" {
#endif


#include <qgl.h>
//#include "swissranger.h" //Ranger Library

#define ONE -125
#define TWO -124
#define THREE -123
#define FOUR 1
#define FIVE 125
#define SIX 124
#define SEVEN 123
#define EIGHT -1

class Rangergl : public QGLWidget
{
public:
    Rangergl(QWidget *parent = 0, const char *name = 0);
	void load_data();
	/*double featureRange;
	double featureBearingX;
	double featureBearingY;
	double featureStatus;
*/
	int noiseFilter;
	double featureBearingX;
	double featureBearingY;
	unsigned short featureRange;
	double featureRangeBest;   
	unsigned short pixels[19840*2];
 	void MytimerEvent( void );
	int closeRanger(void);
	unsigned short *pixPtr;
	double featureStatus;


protected:
    void initializeGL();
    void resizeGL(int width, int height);

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);

   // void MytimerEvent( QTimerEvent * );

	short unsigned int pinchNum(short unsigned int number);
        void drawArrows(double p);
	void featureSearch();
	void drawnFeatureRecticle(double p);
	void drawnFeatureRecticleBest(double p);

	void drawRange(double p);
	double wuzzyFix(unsigned short best, double bestStatus, unsigned short newData, double newDataStatus);

//Variables
	

//	double featureBearingX;
//	double featureBearingY;
//	unsigned short featureRangeBest;

	unsigned short featureRangePast;
	double featureBearingXPast;
	double featureBearingYPast;
	int filter;
	double viewer_offset;

//private slots:
	int getfeatureBearingY(void);

private slots:
    void paintGL();
//	void draw();
private:
   void draw();
    int faceAtPosition(const QPoint &pos);
	int saturationCheck(int k);
	void trackSaturationFeature(int k);
void drawnAwesimo(double p);

	//int len 19840;
	int fd;
	int res;
	int sample_size;
	int normalTimer;
	   float color[3] ;
   GLfloat mat_specular[4] ;
   GLfloat mat_shininess[1] ;

    GLfloat rotationX;
    GLfloat rotationY;
    GLfloat rotationZ;
    QColor faceColors[6];
    QPoint lastPos;
};


#ifdef __cplusplus
}
#endif
#endif

