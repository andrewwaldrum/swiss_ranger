/****************************************************************************
** Form implementation generated from reading ui file 'gui/srGui.ui'
**
** Created: Wed Nov 8 00:55:31 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "srGui.h"

#include <qvariant.h>
#include <qdatetime.h>
#include <qpushbutton.h>
#include <qlcdnumber.h>
#include <qlabel.h>
#include <qcombobox.h>
#include <qslider.h>
#include <qdial.h>
#include <qspinbox.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include "srGui.ui.h"
/*
 *  Constructs a spring01 as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
spring01::spring01( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "spring01" );

    ybearing = new QLCDNumber( this, "ybearing" );
    ybearing->setGeometry( QRect( 620, 250, 104, 28 ) );

    textLabel1_2 = new QLabel( this, "textLabel1_2" );
    textLabel1_2->setGeometry( QRect( 620, 220, 104, 27 ) );

    textLabel1 = new QLabel( this, "textLabel1" );
    textLabel1->setGeometry( QRect( 620, 140, 104, 27 ) );

    DrawButton_2 = new QPushButton( this, "DrawButton_2" );
    DrawButton_2->setGeometry( QRect( 620, 50, 110, 27 ) );

    xbearing = new QLCDNumber( this, "xbearing" );
    xbearing->setGeometry( QRect( 620, 170, 104, 27 ) );

    ResetButton = new QPushButton( this, "ResetButton" );
    ResetButton->setGeometry( QRect( 620, 90, 110, 27 ) );

    textLabel1_3 = new QLabel( this, "textLabel1_3" );
    textLabel1_3->setGeometry( QRect( 620, 287, 104, 27 ) );

    DrawButton = new QPushButton( this, "DrawButton" );
    DrawButton->setGeometry( QRect( 620, 10, 110, 27 ) );

    pixmapLabel1 = new QLabel( this, "pixmapLabel1" );
    pixmapLabel1->setGeometry( QRect( 10, 20, 600, 540 ) );
    pixmapLabel1->setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    pixmapLabel1->setScaledContents( TRUE );

    QWidget* privateLayoutWidget = new QWidget( this, "layout5" );
    privateLayoutWidget->setGeometry( QRect( 0, 570, 454, 67 ) );
    layout5 = new QVBoxLayout( privateLayoutWidget, 11, 6, "layout5"); 

    layout3 = new QHBoxLayout( 0, 0, 6, "layout3"); 

    layout2 = new QHBoxLayout( 0, 0, 6, "layout2"); 

    initialize_button = new QPushButton( privateLayoutWidget, "initialize_button" );
    layout2->addWidget( initialize_button );

    Go_button = new QPushButton( privateLayoutWidget, "Go_button" );
    Go_button->setEnabled( TRUE );
    Go_button->setToggleButton( TRUE );
    Go_button->setAutoRepeat( TRUE );
    Go_button->setAutoDefault( TRUE );
    Go_button->setFlat( FALSE );
    layout2->addWidget( Go_button );

    STOP_button = new QPushButton( privateLayoutWidget, "STOP_button" );
    layout2->addWidget( STOP_button );
    layout3->addLayout( layout2 );

    textLabel4 = new QLabel( privateLayoutWidget, "textLabel4" );
    layout3->addWidget( textLabel4 );

    textLabel3 = new QLabel( privateLayoutWidget, "textLabel3" );
    layout3->addWidget( textLabel3 );
    layout5->addLayout( layout3 );

    layout4 = new QHBoxLayout( 0, 0, 6, "layout4"); 

    OA_selector = new QComboBox( FALSE, privateLayoutWidget, "OA_selector" );
    layout4->addWidget( OA_selector );
    spacer1 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout4->addItem( spacer1 );
    layout5->addLayout( layout4 );

    slider1 = new QSlider( this, "slider1" );
    slider1->setGeometry( QRect( 0, 650, 454, 24 ) );
    slider1->setOrientation( QSlider::Horizontal );

    dial1 = new QDial( this, "dial1" );
    dial1->setGeometry( QRect( 460, 570, 70, 80 ) );
    dial1->setValue( 50 );

    SrangerDisp = new QLCDNumber( this, "SrangerDisp" );
    SrangerDisp->setGeometry( QRect( 470, 650, 104, 27 ) );
    SrangerDisp->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
    SrangerDisp->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
    QFont SrangerDisp_font(  SrangerDisp->font() );
    SrangerDisp_font.setFamily( "Arial Black" );
    SrangerDisp_font.setPointSize( 16 );
    SrangerDisp->setFont( SrangerDisp_font ); 

    RangeDsp = new QLCDNumber( this, "RangeDsp" );
    RangeDsp->setGeometry( QRect( 620, 320, 70, 30 ) );
    RangeDsp->setPaletteForegroundColor( QColor( 255, 100, 0 ) );
    RangeDsp->setPaletteBackgroundColor( QColor( 255, 200, 255 ) );
    QFont RangeDsp_font(  RangeDsp->font() );
    RangeDsp_font.setFamily( "Arial Black" );
    RangeDsp_font.setPointSize( 16 );
    RangeDsp->setFont( RangeDsp_font ); 

    QWidget* privateLayoutWidget_2 = new QWidget( this, "layout5" );
    privateLayoutWidget_2->setGeometry( QRect( 750, 10, 82, 440 ) );
    layout5_2 = new QVBoxLayout( privateLayoutWidget_2, 11, 6, "layout5_2"); 

    headPos_2 = new QLabel( privateLayoutWidget_2, "headPos_2" );
    layout5_2->addWidget( headPos_2 );

    spinBox1_2 = new QSpinBox( privateLayoutWidget_2, "spinBox1_2" );
    spinBox1_2->setMaxValue( 5000 );
    layout5_2->addWidget( spinBox1_2 );

    slider3 = new QSlider( privateLayoutWidget_2, "slider3" );
    slider3->setMaxValue( 5000 );
    slider3->setOrientation( QSlider::Vertical );
    layout5_2->addWidget( slider3 );

    QWidget* privateLayoutWidget_3 = new QWidget( this, "layout5_2" );
    privateLayoutWidget_3->setGeometry( QRect( 840, 10, 82, 440 ) );
    layout5_2_2 = new QVBoxLayout( privateLayoutWidget_3, 11, 6, "layout5_2_2"); 

    headPos_2_2 = new QLabel( privateLayoutWidget_3, "headPos_2_2" );
    layout5_2_2->addWidget( headPos_2_2 );

    spinBox1_2_2 = new QSpinBox( privateLayoutWidget_3, "spinBox1_2_2" );
    layout5_2_2->addWidget( spinBox1_2_2 );

    sliderThrottle = new QSlider( privateLayoutWidget_3, "sliderThrottle" );
    sliderThrottle->setMaxValue( 100 );
    sliderThrottle->setValue( 50 );
    sliderThrottle->setOrientation( QSlider::Vertical );
    layout5_2_2->addWidget( sliderThrottle );

    spinBox1 = new QSpinBox( this, "spinBox1" );
    spinBox1->setGeometry( QRect( 620, 360, 55, 26 ) );
    languageChange();
    resize( QSize(945, 692).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( DrawButton, SIGNAL( clicked() ), this, SLOT( DrawSpring() ) );
    connect( ResetButton, SIGNAL( clicked() ), this, SLOT( Reset() ) );
    connect( STOP_button, SIGNAL( pressed() ), this, SLOT( STOP_button_pressed() ) );
    connect( initialize_button, SIGNAL( pressed() ), this, SLOT( initialize_button_pressed() ) );
    connect( OA_selector, SIGNAL( activated(const QString&) ), textLabel3, SLOT( setText(const QString&) ) );
    connect( dial1, SIGNAL( valueChanged(int) ), this, SLOT( dial1_valueChanged(int) ) );
    connect( OA_selector, SIGNAL( textChanged(const QString&) ), OA_selector, SLOT( setEditText(const QString&) ) );
    connect( OA_selector, SIGNAL( textChanged(const QString&) ), this, SLOT( OA_selector_textChanged(const QString&) ) );
    connect( slider3, SIGNAL( valueChanged(int) ), this, SLOT( slider3_valueChanged(int) ) );
    connect( spinBox1, SIGNAL( valueChanged(int) ), dial1, SLOT( setValue(int) ) );
    connect( dial1, SIGNAL( valueChanged(int) ), spinBox1, SLOT( setValue(int) ) );
    connect( OA_selector, SIGNAL( activated(int) ), this, SLOT( OA_selector_activated(int) ) );
    connect( slider3, SIGNAL( valueChanged(int) ), spinBox1_2, SLOT( setValue(int) ) );
    connect( spinBox1_2, SIGNAL( valueChanged(int) ), slider3, SLOT( setValue(int) ) );
    connect( sliderThrottle, SIGNAL( valueChanged(int) ), spinBox1_2_2, SLOT( setValue(int) ) );
    connect( spinBox1_2_2, SIGNAL( valueChanged(int) ), sliderThrottle, SLOT( setValue(int) ) );
    connect( spinBox1_2_2, SIGNAL( valueChanged(int) ), sliderThrottle, SLOT( setValue(int) ) );
    connect( sliderThrottle, SIGNAL( valueChanged(int) ), spinBox1_2_2, SLOT( setValue(int) ) );
    connect( sliderThrottle, SIGNAL( valueChanged(int) ), this, SLOT( sliderThrottle_valueChanged(int) ) );
    connect( Go_button, SIGNAL( clicked() ), this, SLOT( Go_button_clicked() ) );
    connect( slider1, SIGNAL( valueChanged(int) ), this, SLOT( slider1_valueChanged(int) ) );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
spring01::~spring01()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void spring01::languageChange()
{
    setCaption( tr( "Spring" ) );
    textLabel1_2->setText( tr( "Y bearing" ) );
    textLabel1->setText( tr( "Xbearing" ) );
    DrawButton_2->setText( tr( "Draw Features" ) );
    ResetButton->setText( tr( "Reset" ) );
    textLabel1_3->setText( tr( "Range" ) );
    DrawButton->setText( tr( "Draw Range" ) );
    initialize_button->setText( tr( "Initialize" ) );
    Go_button->setText( tr( "Run OA" ) );
    STOP_button->setText( tr( "STOP" ) );
    textLabel4->setText( tr( "Program:" ) );
    textLabel3->setText( tr( "NULL" ) );
    OA_selector->clear();
    OA_selector->insertItem( tr( "Basic OA" ) );
    OA_selector->insertItem( tr( "BestEverOA" ) );
    OA_selector->insertItem( tr( "Demo" ) );
    OA_selector->insertItem( tr( "FollowingF" ) );
    OA_selector->insertItem( tr( "FollowH" ) );
    OA_selector->insertItem( tr( "OAFeatureF" ) );
    headPos_2->setText( tr( "Filter Value" ) );
    headPos_2_2->setText( tr( "Z scale" ) );
}

