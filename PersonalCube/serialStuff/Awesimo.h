 // gcc -L . -lswissranger -Wall -O3 -o stop stop.c
/*awesimo.h
 * by Andrew Waldrum 4/13/06
 * http://www.csem.ch/fs/copyright.htm
 */


/*
 * MODIFICATION HISTORY: 
 *
 */

#ifndef AWESIMO_H
#define AWESIMO_H

#include <stdio.h>
//#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "serial-io.h"   //Talks to serial servo conttroller
//#include "serial.h" //Testing
#include <unistd.h>     //Not sure what this is

#include <sys/times.h>  // For timing function
//#include <asm/param.h>  // For HZ definition
//#include "swissranger.h" //Ranger Library

class Awesimo {

public:

/*  The servo numbers are identical to channels on stock E-maxx:
        Channel 1 = steering servo
        Channel 2 = speed controller
        Channel 3 = transmission servo
*/

#define      SERVO1_ID              1
#define      SERVO1_MIN             90
#define      SERVO1_MAX             186

#define      SERVO2_ID              2
#define      SERVO2_MIN             0
#define      SERVO2_MAX             255

#define      SERVO3_ID              3
#define      SERVO3_MIN		    0
#define      SERVO3_MAX		    255

/*
// unsigned short pix;
slow = 52;  //throttle settings ,barely moving
brake = 48;//Brakes
hard_left= 0;//Steering settings
hard_right = 100;
centered = 50;*/
/***   Relevant constants   ***/
#define CENTERED 50
#define  HARD_RIGHT 100
#define  HARD_LEFT  0
#define  BRAKE  34
#define  SLOW  52
#define  REVERSE  29
#define NEUTRAL 50

  // File descriptor to the Swiss Ranger
  int fd;

// Counter
int i;

int throttle, steering;//these values are the current servo settings
//int t_incriment;
int *steering_ptr,*throttle_ptr;

int s_incriment;//the steering incriment 

//int throttle_g;//These are the goal throttle and steering settings 
int throttle_subslow;//new modifier
int throttle_counter;// counts steps in 

  
// Stores return values
int res;
int running_time; //in seconds
int behavior;
int image_size, sample_size;
//This sets the size of the image. X2 because unsigned short takes 2 bytes
//image_size=SWISSRANGER_ROWS*SWISSRANGER_COLUMNS*2;
//sample_size=image_size*2;
// An array to store pixels acquired by the Swiss Ranger
//  unsigned short pixels [sample_size];//raw data
 // unsigned short image [image_size];//intensity data
  //unsigned short range [image_size];//range data
 // unsigned short calibration [image_size];//calibration of flat plane

  unsigned short *pix_pointer;//pointer to the image
  unsigned short *image_ptr;
  unsigned short *range_ptr;
  unsigned short *calibration_ptr;//pointer to the image

// Used to time a measurement
  struct tms tmsPointer;
  double ticc,tocc;

  SERIAL_DEVICE dev;


	int steering_g;  //Steering goal  
	int throttle_g;//These are the goal throttle and steering settings 
        int rpix, lpix,cpix;
	unsigned short  Fl;//left and right pixels
	unsigned short  Fr, C;
	int x1, x2, x3;   //States
	int S1, S2, S3, S4,  S5, S6, S7, S8;   //conditions T - F;	
	int intensity_limit;	


/* Funtion Prototypes */ 
Awesimo(void);


void startup(void);
void setSerialDevice(SERIAL_DEVICE *dev);
void startSerialDevice( SERIAL_DEVICE *dev );
int steering_set(int fd, int servoValue);
int write_minissc_command(int fd, unsigned char motor, unsigned char position);
int board_set(int fd, int servo1Value, int servo2Value);
int gear_ratio_set(int fd, int servoValue);
int throttle_set(int fd, int servoValue);
int head_set(int fd, int servoValue);

unsigned char getSteeringValue(double valDouble);
//int throttle_set(int fd, int servoValue);
/*
int pixel_preprocess();
unsigned short  pixel_ave(int pixc);
//float get_patch(int staart_point, int p_size, float res);//averages a patch of the data
void basic_OA();	
void better_OA();	
void callibrater(unsigned short *calibration_ptr, int fd, int sample_size);	
void best_OA();
void best_ever_OA();	
void steering_check()
void throttle_check()
*/
};
#endif //Awesimo_h
