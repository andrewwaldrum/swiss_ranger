cmake_minimum_required(VERSION 2.6)
project(swissRangerCapture)

set( CMAKE_CXX_FLAGS "-g -Wall" )
set( BUILD_SHARED_LIBS ON )
set(CMAKE_BINARY_PATH $ENV{CMAKE_BINARY_PATH})

#add source files here. 
set( 
    APP_SRC 
    swissrangerTester.c 
    )

#gets an absolute path.  to the driver. 
get_filename_component(DRIVER_DIR  ../driver/ REALPATH)
#message("DRIVER_DIR is ${DRIVER_DIR}")

include_directories( 
    ${PROJECT_SOURCE_DIR} 
    ${DRIVER_DIR} 
    )

#where libraries will be found, .so's .a's etc. 
link_directories(
    ${DRIVER_DIR} 
    )

add_executable(
    swissRangerCapture 
    ${APP_SRC}
    )

#equivalent to -l(libname)
target_link_libraries(
    swissRangerCapture
    swissranger
    usb
    )

#Not really used yet. 
install(
    TARGETS swissRangerCapture
    DESTINATION ${CMAKE_BINARY_PATH}
    )


set( EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin )
