/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/
//Note: spring01 is the Gui adapted from another example, it reemains spring01 for simplicity
//pspring is the opengl opbject
void spring01::init()
{
    //Awesimo *awesimo= new Awesimo;
  Awesimo awesimo;
  
  //awesimo.startup();
    pSpring = 0; //Initiaize always tge pointer
    //W = 0;
    
}

void spring01::DrawSpring()
{
    // Free memory if a spring object already exists.
    if(pSpring != 0)
    {
	delete pSpring;
    }
    
    // Allocate a new OopenGL object
    pSpring = new Rangergl(pixmapLabel1, 0);
    
    // Display the spring.
    pSpring->resize(pixmapLabel1->size());
    pSpring->pixPtr= RangeObj.pixels;;//rangePtr;
    //int normalTimer;
//    normalTimer=startTimer(100);

    pSpring->show();
  

}


void spring01::timerEvent( QTimerEvent *e )
{
    
    //take data
    
    	 RangeObj.MytimerEvent();
    //render data
	 rangePtr= RangeObj.pixels;
	 
	 //This only runs opengl stuff if it is created, Program can be run without 
	 if(pSpring != 0)
	 {
	     pSpring->MytimerEvent();

	    // ybearing->display(int(pSpring->featureBearingY));
	     //xbearing->display(int(pSpring->featureBearingX));
	   //  RangeDsp->display(int(.04335*pSpring->featureRangeBest-160));
	    // W.featureX= int(pSpring->featureBearingX);	
	     //W.featureY= int(pSpring->featureBearingY);
	     //W.featureRange=int(.09824*pSpring->featureRangeBest-167.6);
	     //W.featureRange=int(pSpring->featureRangeBest);
	    ///W.featureRange=int(0.042587*pSpring->featureRangeBest-3000);
	     //    rangePtr=pSpring->pixels;
	     
	     pSpring->featureRangeBest=W.featureRangeBest;
	     pSpring->featureRange=W.featureRange;
	     pSpring->featureBearingY= W.featureBearingY;
	     pSpring->featureBearingX= W.featureBearingX;
	     pSpring->featureStatus = W.featureStatus;
	 }	
	 
	// RangeDsp->display(int(.04335*W.featureRangeBest-160));	 	 	
	 RangeDsp->display(int(.0154*W.featureRangeBest-35));
	 //RangeDsp->display(int(W.featureRangeBest));
	 SrangerDisp->display(int(W.featureBearingY) );
	 ybearing->display(int(W.featureBearingY));
	 xbearing->display(int(W.featureBearingX));
	
     
	 //pSpring->pixels = RangeObj.pixels;
	 //W.pixPtr=RangerObj.pixels;
	 W.pixPtr=rangePtr;
	 W.decide();
	 awesimo.steering_set(awesimo.dev.fd,W.steeringSetting);   
	//steer full rightsteeringSetting;
	 awesimo.throttle_set(awesimo.dev.fd,W.throttleSetting);   
	awesimo.head_set(awesimo.dev.fd,W.headSetting); 
	spinBox1->setValue(W.headSetting);
	//pSpring->swissranger_acquire(fd, pixels, sample_size);
      	//pSpring->updateGL();
		
   
}

void spring01::Reset()
{
    // Free memory if a spring object already exists.
    if(pSpring != 0)
    {         
	pSpring->closeRanger();
	delete pSpring;
	//normalTimer=stopTimer(100);
	//pSpring = 0;
    }
}


void spring01::dial1_valueChanged( int z)
{
awesimo.head_set(awesimo.dev.fd,dial1->value());  
W.headSetting=z;
}

void spring01::slider1_valueChanged( int z )
{
//awesimo.head_set(awesimo.dev.fd,slider1->value());  
W.headSetting=z;
    awesimo.steering_set(awesimo.dev.fd,z);   //steer full right
}


void spring01::STOP_button_pressed()
{
    awesimo.steering_set(awesimo.dev.fd,50);   //steer full right
 awesimo.throttle_set(awesimo.dev.fd,34);    /* go forward as slow as  */
awesimo.head_set(awesimo.dev.fd,50);  
}


void spring01::initialize_button_pressed()
{
   
 awesimo.startup();
  awesimo.steering_set(awesimo.dev.fd,50);   //steer full right
 awesimo.throttle_set(awesimo.dev.fd,34);    /* go forward as slow as  */
 awesimo.head_set(awesimo.dev.fd,66);
 wDecider W;
 W.headSetting=64;
 RangeObj.load_data();
 W.pixPtr=rangePtr;

//  W.decide(void)=W.decideTrack(void);
 //int normalTimer;
//normalTimer=
//startTimer(100);int normalTimer;

//    normalTimer=startTimer(100);

 
}

void spring01::OA_selector_textChanged( const QString & )
{
    awesimo.steering_set(awesimo.dev.fd,50);   	
//    string AA;
//AA=OA_selector.returnPressed()      
/*    if (OA_selector.returnPressed()  == "FollowH")
    {
	
   awesimo.steering_set(awesimo.dev.fd,50);   	
    }
   */ 
}

void spring01::slider3_valueChanged( int z)
{
pSpring->noiseFilter=slider3->value();
W.noiseFilter=slider3->value();
//W.headSetting=z;
}

void spring01::OA_selector_activated( int z)
{
W.selector=z;
}

void spring01::sliderThrottle_valueChanged( int z)
{
 awesimo.throttle_set(awesimo.dev.fd,z);   
}

//Starts the timer not the timer event will now execute
void spring01::Go_button_clicked()
{
int normalTimer;

    normalTimer=startTimer(200);

}
