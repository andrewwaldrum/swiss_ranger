/* Test the swissranger user-space Linux driver.
 *
 * Assuming the libswissranger.so library is in the current directory,
 * compile as:
 *
 * gcc -L . -lswissranger -Wall -O3 -o swissrangerTester swissrangerTester.c
 *
 * @version 1.2, 14.2.05
 * @author Gabriel Gruener, gabriel.gruener@csem.ch
 */

#include "swissranger.h"

#include <stdio.h>
#include <unistd.h>
#include <sys/times.h>  // For timing function
#include <asm/param.h>  // For HZ definition

int main() {
    printf("start main...\n");
    // Counter
    int i;

    // File descriptor to the Swiss Ranger
    int fd;

    // Stores return values
    int res;

//This was 2, might need to be 4 depending on how unsigned short are represented in arm achitecture. 
    const int bufferMultuplier = 4;

    const int bufferSize = SWISSRANGER_ROWS * SWISSRANGER_COLUMNS * bufferMultuplier;

    // An array to store pixels acquired by the Swiss Ranger
    unsigned short pixels [bufferSize];

    // Used to time a measurement
    struct tms tmsPointer;
    clock_t tic, toc;

    printf("Trying to open Ranger from test app.\n");

    // Open device
    printf("Attempting to open Swiss Ranger device\n");
    if ((fd = swissranger_open()) < 0) {

        printf("Couldn't connect to Swiss Ranger device\n");
        return -1;
    }
    printf("Device opened successfully\n");

    // Send configuration information: Read camera's documentation
/*
     res = swissranger_send(fd, 0x2, 0x40);    // Want a real measurement, or
    res = swissranger_send(fd, 0x2, 0x41);    // Want test pattern
     if (res < 0)
         printf("Failed to send configuration");
*/

    // Get one measurement
    printf("Attempting to get 1 measurement\n");
    tic = times(&tmsPointer);
    res = swissranger_acquire(fd, pixels, bufferSize);
    res = swissranger_acquire(fd, pixels, bufferSize);
    toc = times(&tmsPointer);

    if (res < 0) 
    {

        printf("Failed to acquire the measurement");

    } 
    else 
    {

        printf("%d bytes read (expected %d)", res, SWISSRANGER_ROWS*SWISSRANGER_COLUMNS*2*2);
        printf("Measurement successful in %fs. Dumping image to stdout in octave format", 1.0*(toc-tic)/HZ);

        printf("# Created by " __FILE__ " for octave (akin Matlab)\n");
        printf("# name: distance\n");
        printf("# type: matrix\n");
        printf("# rows: %d\n", SWISSRANGER_ROWS);
        printf("# columns: %d\n", SWISSRANGER_COLUMNS);
        for (i=0; i<SWISSRANGER_ROWS*SWISSRANGER_COLUMNS; i++) 
        {
            if (i != 0 && i%(SWISSRANGER_COLUMNS) == 0)
            {
                printf("\n");
            }
            printf("%u ", (pixels[i]));
        }

        printf("\n\n");
        printf("# name: intensity\n");
        printf("# type: matrix\n");
        printf("# rows: %d\n", SWISSRANGER_ROWS);
        printf("# columns: %d\n", SWISSRANGER_COLUMNS);
        for ( ; i<SWISSRANGER_ROWS*SWISSRANGER_COLUMNS*2; i++) {

            if (i%(SWISSRANGER_COLUMNS) == 0)
                printf("\n");
            printf("%u ", (pixels[i]));
        }

        printf("\n");
    }

    // Close device
    printf("Attempting to close Swiss Ranger device");
    if (swissranger_close(fd) < 0) {

        printf("Couldn't close Swiss Ranger device properly");
        return -1;
    }
    return 0;
}
