#include <qcolordialog.h>
//#include <GL/gl.
//#include <unistd>     //Not sure what this is
#include <fstream>
#include <string>
#include <iostream>
#include <math.h>
//#include <stdio.h>
//#include <stdlib.h>
#include <qdatetime.h>

#include "rangergl.h"
//#include "swissranger.h" //Ranger Library
//#include "awesimo.h"  //My values and functions

Rangergl::Rangergl(QWidget *parent, const char *name)
    : QGLWidget(parent, name)
{
    setFormat(QGLFormat(DoubleBuffer | DepthBuffer));
	//pixels[0]=0;
    rotationX = 5;
    rotationY = 10;//180
    rotationZ = 0;
featureRange=0;
featureRangeBest=0; 
//normalTimer=startTimer(100);
   color[0] = 1.0 ; color[1] = 0.0 ; color[2] = 0.0 ;  //red

   mat_specular[0] = 1.0 ;
   mat_specular[1] = 1.0 ;
   mat_specular[2] = 1.0 ;
   mat_specular[3] = 1.0 ;
   mat_shininess[0] =  50.0 ;

}

//Opens Device 
///////////////////////////Move from render
void Rangergl::load_data()
{   
featureStatus=0;
//	sample_size=SWISSRANGER_ROWS*SWISSRANGER_COLUMNS*2*2;
//	if ((fd = swissranger_open()) < 0) 
//	{  
//		printf("Couldn't connect to Swiss Ranger device") ;
//		//return -1;
  //	}
	//printf("Device opened successfully");
//	res=swissranger_acquire(fd, pixels, sample_size);
//	res=swissranger_acquire(fd, pixels, sample_size);

//	printf("pixel 1 is %u \n",pixels[9920]);//was 80

}

//OpenGL initialization
void Rangergl::initializeGL()
{
	load_data();
	qglClearColor(black);
	glShadeModel(GL_FLAT);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
}

//OpenGL resize
void Rangergl::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
//    GLfloat x = (GLfloat)width / height;
//  glFrustum(-x, x, 0.0, 200.0, 0.0, 100000.0);
//     //glFrustum(-x, x, -1.0, 1.0, 2.0, 20.0);
gluPerspective(45,2,.5,100.0);
//glFrustum(-80,80,-80,80,4,65530);
    glMatrixMode(GL_MODELVIEW);
}



void Rangergl::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    draw();
}


//
// Handles timer events for the digital clock widget.
// There are two different timers; one timer for updating the clock
// and another one for switching back from date mode to time mode.
//
//This should be moved "upward" to the calling object so the same timer can be used to 

//Timed stuff that is activated by a timer in the calling program
//void Rangergl::MytimerEvent( QTimerEvent *e )
void Rangergl::MytimerEvent(void)

{
//	featureSearch();
//	res=swissranger_acquire(fd, pixels, sample_size);
      	updateGL();
}


void Rangergl::draw()
{

double p;
p =4.0;
viewer_offset =-15.0;

	//res=swissranger_acquire(fd, pixels, sample_size);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0, 0.0, viewer_offset);
    glRotatef(rotationX, 1.0, 0.0, 0.0);
    glRotatef(rotationY, 0.0, 1.0, 0.0);
    glRotatef(rotationZ, 0.0, 0.0, 1.0);

//Draw the arrows...
drawArrows(p);
//Draw Feature recticle...
//featureSearch(p);
drawnFeatureRecticle(p);
drawnFeatureRecticleBest(p);
//Draw the range pixels...
drawRange(p);
drawnAwesimo(p);
}

void Rangergl::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->pos();
}

void Rangergl::mouseMoveEvent(QMouseEvent *event)
{
    GLfloat dx = (GLfloat)(event->x() - lastPos.x()) / width();
    GLfloat dy = (GLfloat)(event->y() - lastPos.y()) / height();

    if (event->state() & LeftButton) {
        rotationX += 180 * dy;
        rotationY += 180 * dx;
        updateGL();
    } else if (event->state() & RightButton) {
        rotationX += 180 * dy;
        rotationZ += 180 * dx;
        updateGL();
    }
    lastPos = event->pos();
}

void Rangergl::mouseDoubleClickEvent(QMouseEvent *event)
{
    int face = faceAtPosition(event->pos());
    if (face != -1) {
        QColor color = QColorDialog::getColor(faceColors[face],
                                              this);
        if (color.isValid()) {
            faceColors[face] = color;
            updateGL();
        }
    }
}

int Rangergl::faceAtPosition(const QPoint &pos)
{
    const int MaxSize = 512;
    GLuint buffer[MaxSize];
    GLint viewport[4];

    glGetIntegerv(GL_VIEWPORT, viewport);
    glSelectBuffer(MaxSize, buffer);
    glRenderMode(GL_SELECT);

    glInitNames();
    glPushName(0);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluPickMatrix((GLdouble)pos.x(),
                  (GLdouble)(viewport[3] - pos.y()),
                  10.0, 10.0, viewport);
   //Lfloat x = (GLfloat)width() / height();
  //  glFrustum(-x, x, -2.0, 2.0, 2.0, 18.0);
   // gluPerspective(45,1.0,.5,30.0);
//    glFrustum(-40.0, 200.0, -20.0, 140.0, 0.0, 65536.0);

    draw();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    if (!glRenderMode(GL_RENDER))
        return -1;
    return buffer[3];
}

/*uses bit shifting to remove 8th bit from unsigned short number
Also removes empty 1st and 2nd bits.
*/
short unsigned int Rangergl::pinchNum(short unsigned int number) {
	/*short unsigned int lowNum;
	short unsigned int hiNum;
	lowNum=number;//1111111X111111XX
	hiNum=number;//1111111X11111100
	lowNum <<= 8;//X|111111XX|00000000>x gone
	lowNum >>= 10;//0000000000111111|XX->XXgone
//	lowNum >>= 16;//0000000000111111|XX->XXgone

	hiNum= hiNum >> 9;//0000000001111111|X->x gone
	hiNum= hiNum << 6;//001111111000000
	number =hiNum+lowNum;//0001111111111111
*/	
	number =number >>2;
	//number =number <<4;

	return number;
}


//Draw the arrows
//Draws orthogonal colored direction arrows from the origin of an openGL view.  
//p is a scaling factor 
void Rangergl::drawArrows(double p)
{
double arrow;
double arrowL,arrowHead;
arrow=1.0;
arrowL=arrow*p;
arrowHead=arrowL/60;
//Draw the arrows

glBegin(GL_LINES);
glColor3f(0,.5,.7);
glVertex3f(arrowL, 0,0);
glVertex3f(0, 0,0);
glVertex3f(arrowL, 0,0);
glVertex3f(arrowL-arrowHead*2, -arrowHead,arrowHead);
glVertex3f(arrowL, 0,0);
glVertex3f(arrowL-arrowHead*2, arrowHead,-arrowHead);


glColor3f(.7,0,0);
glVertex3f(0,arrowL,0);
glVertex3f(0,0,0);
glVertex3f(0,arrowL,0);
glVertex3f(-arrowHead,arrowL-arrowHead*2, arrowHead);
glVertex3f(0,arrowL, 0);
glVertex3f(arrowHead,arrowL-arrowHead*2, -arrowHead);


glColor3f(0,.7,0);
glVertex3f( 0,0, arrow*p);
glVertex3f(0, 0,0);
glVertex3f(0,0,arrow*p);
glVertex3f(-arrowHead,arrowHead, arrow*p-arrowHead*2);
glVertex3f(0,0,arrow*p);
glVertex3f(arrowHead,-arrowHead, arrow*p-arrowHead*2);

glEnd();
}

/*finds reflector in field of view
Uses and changes 
	double featureRange;
	double featureBearingX;
	double featureBearingY;
	double featureStatus;
*/
void Rangergl::featureSearch()
{

int k;
//featureRange = 5;
featureStatus=0;

  for (int i = 0; i < 160 && featureStatus==0 ; ++i) {
        for (int j = 0; j < 124 && featureStatus==0 ; ++j) {

		k=j+124*i;
	
	//sets satruated pixels to light blue and place them before the scene.
		if (saturationCheck(k))
		{
		trackSaturationFeature(k);
		featureBearingX=i;
		featureBearingY=j;
	 	featureStatus=1;
        	}//end 1-124 loop
	
	}
if(i >=159){featureStatus=0;}
}

//if(i >=160 && j >=124)
featureBearingXPast=featureBearingX;//double(i)*(p/160);
featureBearingYPast=featureBearingY;// 
featureRangePast = featureRange;
featureRangeBest= int(wuzzyFix(featureRangeBest, 3,pinchNum(featureRange),1));
}

/*Do a "wall following " of the saturated patch and record path
*/
void Rangergl::trackSaturationFeature(int k)
{
int startPoint = k;
//int x,y;
//int pos;
//double bestBearingX;
//double bestBearingY;
unsigned short int bestRange;
int dirs[8];
unsigned short int dirCount;

dirCount = 1;

dirs[0]=-125;
dirs[1]=-124;
dirs[2]= -123;
dirs[3]= 1;
dirs[4]= 125;
dirs[5]= 124;
dirs[6]= 123;
dirs[7]= -1;
//pos = dirs[dirCount];
dirCount++;
//bestRange=0;

//range=pixels[pos];
//intense=pixels[pos+19840];
//bestRange= pixels[startPoint+dirs[1]+dirs[1]];
//bestRange= pixels[startPoint+dirs[1]];
bestRange= *(pixPtr+startPoint+dirs[1]);

featureRange=bestRange; //int(wuzzyFix(featureRange, 6, bestRange,1));
//bestRange;

//featureBearingX=int(bestBearingX);
//featureBearingY=int(bestBearingY);
}

int Rangergl::saturationCheck(int k)
	{
	int sat=0; 
	//if (pixels[k+19840]==65532)
	if (*(pixPtr+k+19840)==65532)

		{sat=1;}
	return sat;
}

void Rangergl::drawRange(double p)
{
double ii,jj;//kk;
int k;
double pix;
float color;
double xpos,ypos,z,r;
//short unsigned int *px;
float theeta, phii;
filter = 2500;

glPointSize(2);
glBegin(GL_POINTS);

  for (int i = 0; i < 160; ++i) {

        for (int j = 0; j < 124; ++j) {

		k=j+124*i;

		//pinchnum fixes eigth bit problem
		pix = double(pinchNum(*(pixPtr+k)));// /400
		theeta =.01945*.25*double(i-80);
	     //   fprintf(stderr, "theta II is %f\n",theeta);
		phii = .1854*double(j-62)*.01745;
 	//	phii = 20/62*double(j-62)*.01745;


		r = (.0154*pix-35)/4;

		xpos = r*sin(theeta);
		ypos = r*sin(phii);
		z = r*cos(1.2*theeta)*cos(1.6*phii);
//		z = r;
//fprintf(stderr, "Z is %f\n",z);
	//filter level..puts pixels with intientisty values below a certain value at back of display
		if (*(pixPtr+k+19840)<=noiseFilter)//filter
		{
		//pix = 23;
   		//glColor3f(.5,.5,0);
		} else if (saturationCheck(k))
		{

//sets satruated pixels to light blue and place them before the scene.
//z = 14-double(pinchNum(featureRange))/400;
		pix=double(pinchNum(featureRange));
               // z = 14-double(featureRangeBest)/400;
		r = (.0154*pix-35)/4;
		z = r*cos(theeta)*cos(phii);
		fprintf(stderr, "Z is %f\n",9-z);

  	 	glColor3f(.5,1,1);
//		glVertex3f(-p/2+ii,-p/2+jj,z);  //-16
		glVertex3f(xpos,ypos,z);  //-16
		}else{//normal pixel values
			//color=float(*(pixPtr+k+19840))/65536;
			color=float(*(pixPtr+k+19840))/65536;

		glColor3f(color+.2,color+.2,color+.2);//the .3 just looks good, making the noise visible
		//glVertex3f(xpos,ypos,14-pix);  //-16
		glVertex3f(xpos,ypos,9-z);  //-16

		
	}
        	}//end 1-124 loop
	}
  //  fprintf(stderr, "X is %d\n", x);

        glEnd();
}

void Rangergl::drawnFeatureRecticle(double p)
{
 glBegin(GL_LINES);

double cross;
//double arrowL,arrowHead;
double x,y,z;
cross=.2*p;
//arrowL=cross*p;
//arrowHead=arrowL/60;
x=double(featureBearingX)*(p/160);
y=double(featureBearingY)*(p/160);

z = 14-double(pinchNum(featureRange))/400;

//Draw the arrows
if(featureStatus==0){
glColor3f(.6,.6,1);
	}else{
//glColor3f(.6,.6,0);
//glColor3f(1,0,1);
glColor3f(1,1,0);
glVertex3f(-p/2+x-cross/2,-p/2+y+cross/2, z);
glVertex3f(-p/2+x-cross/2,-p/2+ y-cross/2, z);
glVertex3f(-p/2+x+cross/2,-p/2+ y-cross/2, z);
glVertex3f(-p/2+x+cross/2, -p/2+y+cross/2, z);
glVertex3f(-p/2+x+cross/2,-p/2+y+cross/2, z);
glVertex3f(-p/2+x-cross/2,-p/2+ y+cross/2, z);
glVertex3f(-p/2+x-cross/2,-p/2+ y-cross/2, z);
glVertex3f(-p/2+x+cross/2, -p/2+y-cross/2, z);

}

glEnd();

}
	


void Rangergl::drawnFeatureRecticleBest(double p)
{
 glBegin(GL_LINES);

double cross;
//double arrowL,arrowHead;
double x,y,z;
cross=.3*p;
//arrowL=cross*p;
//arrowHead=arrowL/60;
x=double(featureBearingX)*(p/160);
y=double(featureBearingY)*(p/160);

z = 14-double(featureRangeBest)/400;

//Draw the arrows
if(featureStatus==0){
glColor3f(.6,.6,0);
	}else{


glColor3f(1,1,0);
glVertex3f(-p/2+x-cross/2,-p/2+y+cross/2, z);
glVertex3f(-p/2+x-cross/2,-p/2+ y-cross/2, z);
glVertex3f(-p/2+x+cross/2,-p/2+ y-cross/2, z);
glVertex3f(-p/2+x+cross/2, -p/2+y+cross/2, z);
glVertex3f(-p/2+x+cross/2,-p/2+y+cross/2, z);
glVertex3f(-p/2+x-cross/2,-p/2+ y+cross/2, z);
glVertex3f(-p/2+x-cross/2,-p/2+ y-cross/2, z);
glVertex3f(-p/2+x+cross/2, -p/2+y-cross/2, z);

}
//featureRange=pinchNum(featureRange);

glVertex3f(-p/2+x-cross,-p/2+y, z);
glVertex3f(-p/2+x+cross,-p/2+y, z);
glVertex3f(-p/2+x,-p/2+ y-cross, z);
glVertex3f(-p/2+x, -p/2+y+cross, z);
glEnd();

}

void Rangergl::drawnAwesimo(double p)
{
   glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
   glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
   glMaterialfv(GL_FRONT, GL_DIFFUSE, color);
glBegin(GL_POLYGON) ;
       glNormal3d(0.0, 0.0, 1.0) ;

glEnd();

}
	

int Rangergl::getfeatureBearingY(void)
{
return int(featureBearingY);
}

double Rangergl::wuzzyFix(unsigned short best, double bestStatus, unsigned short newData, double newDataStatus)
{
double wuzzy,worth;
double w1,w2;

w1=best*bestStatus;
w2=newData*newDataStatus;
worth = bestStatus+newDataStatus;
//worth= newDataStatus/bestStatus;
//wuzzy=best-(best-newData)*worth;
wuzzy = (w1+w2)/worth;
return wuzzy;
}

int Rangergl::closeRanger(void)
{
//if (swissranger_close(fd) < 0) {
//
		//err("Couldn't close Swiss Ranger device properly");
//		return -1;
//	}
return 0;
}
