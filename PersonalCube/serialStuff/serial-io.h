#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif
//#ifndef SERIAL-IO_H
//#define SERIAL-IO_H

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_NAME_LENGTH             256

#define DEFAULT_SERIAL_PORT                      "/dev/ttyS0"    
//"/dev/ttyS2"
#define DEFAULT_SERIAL_BAUD                      9600
#define DEFAULT_SERIAL_PARITY                    N
#define DEFAULT_SERIAL_DATA_BITS                 8
#define DEFAULT_SERIAL_STOP_BITS                 1
#define DEFAULT_SERIAL_HWF                       FALSE
#define DEFAULT_SERIAL_SWF                       FALSE

#define BUFFER_SIZE                      8196
#define MAX_COMMAND_SIZE                 8196

enum PARITY_TYPE   { N, E, O };

#ifndef TIOCGETP 

#define TIOCGETP                      0x5481
#define TIOCSETP                      0x5482
#define RAW                                1
#define CBREAK                            64

#endif

typedef struct {
  char               * ttyport;
  int                  baud;
  enum PARITY_TYPE     parity;
  int                  fd;
  int                  databits;
  int                  stopbits;
  int                  hwf;
  int                  swf;
} SERIAL_DEVICE;

int     DEVICE_connect_port( SERIAL_DEVICE *dev );
void    DEVICE_set_params( SERIAL_DEVICE dev );
void    DEVICE_set_baudrate( SERIAL_DEVICE dev, int baudrate );
int writeData( int fd, unsigned char *buf, int nChars );

//#endif //serial-io.h
#ifdef __cplusplus
}
#endif

