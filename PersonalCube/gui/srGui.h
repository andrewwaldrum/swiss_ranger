/****************************************************************************
** Form interface generated from reading ui file 'gui/srGui.ui'
**
** Created: Wed Nov 8 00:55:18 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef SPRING01_H
#define SPRING01_H

#include <qvariant.h>
#include <qdialog.h>
#include "rangergl.h"
#include "Awesimo.h"
#include "wDecider.h"
#include "Sranger.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLCDNumber;
class QLabel;
class QPushButton;
class QComboBox;
class QSlider;
class QDial;
class QSpinBox;

class spring01 : public QDialog
{
    Q_OBJECT

public:
    spring01( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~spring01();

    QLCDNumber* ybearing;
    QLabel* textLabel1_2;
    QLabel* textLabel1;
    QPushButton* DrawButton_2;
    QLCDNumber* xbearing;
    QPushButton* ResetButton;
    QLabel* textLabel1_3;
    QPushButton* DrawButton;
    QLabel* pixmapLabel1;
    QPushButton* initialize_button;
    QPushButton* Go_button;
    QPushButton* STOP_button;
    QLabel* textLabel4;
    QLabel* textLabel3;
    QComboBox* OA_selector;
    QSlider* slider1;
    QDial* dial1;
    QLCDNumber* SrangerDisp;
    QLCDNumber* RangeDsp;
    QLabel* headPos_2;
    QSpinBox* spinBox1_2;
    QSlider* slider3;
    QLabel* headPos_2_2;
    QSpinBox* spinBox1_2_2;
    QSlider* sliderThrottle;
    QSpinBox* spinBox1;

public slots:
    virtual void DrawSpring();
    virtual void timerEvent( QTimerEvent * e );
    virtual void Reset();
    virtual void dial1_valueChanged( int z );
    virtual void slider1_valueChanged( int z );
    virtual void STOP_button_pressed();
    virtual void initialize_button_pressed();
    virtual void OA_selector_textChanged( const QString & );
    virtual void slider3_valueChanged( int z );
    virtual void OA_selector_activated( int z );
    virtual void sliderThrottle_valueChanged( int z );
    virtual void Go_button_clicked();

protected:
    Awesimo awesimo;
    Rangergl *pSpring;
    wDecider W;
    unsigned short *rangePtr;
    SRanger RangeObj;

    QVBoxLayout* layout5;
    QHBoxLayout* layout3;
    QHBoxLayout* layout2;
    QHBoxLayout* layout4;
    QSpacerItem* spacer1;
    QVBoxLayout* layout5_2;
    QVBoxLayout* layout5_2_2;

protected slots:
    virtual void languageChange();

private:
    void init();

};

#endif // SPRING01_H
