/****************************************************************************
** spring01 meta object code from reading C++ file 'srGui.h'
**
** Created: Wed Nov 8 00:55:44 2006
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.4   edited Jan 21 18:14 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "srGui.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *spring01::className() const
{
    return "spring01";
}

QMetaObject *spring01::metaObj = 0;
static QMetaObjectCleanUp cleanUp_spring01( "spring01", &spring01::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString spring01::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "spring01", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString spring01::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "spring01", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* spring01::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QDialog::staticMetaObject();
    static const QUMethod slot_0 = {"DrawSpring", 0, 0 };
    static const QUParameter param_slot_1[] = {
	{ "e", &static_QUType_ptr, "QTimerEvent", QUParameter::In }
    };
    static const QUMethod slot_1 = {"timerEvent", 1, param_slot_1 };
    static const QUMethod slot_2 = {"Reset", 0, 0 };
    static const QUParameter param_slot_3[] = {
	{ "z", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_3 = {"dial1_valueChanged", 1, param_slot_3 };
    static const QUParameter param_slot_4[] = {
	{ "z", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_4 = {"slider1_valueChanged", 1, param_slot_4 };
    static const QUMethod slot_5 = {"STOP_button_pressed", 0, 0 };
    static const QUMethod slot_6 = {"initialize_button_pressed", 0, 0 };
    static const QUParameter param_slot_7[] = {
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_7 = {"OA_selector_textChanged", 1, param_slot_7 };
    static const QUParameter param_slot_8[] = {
	{ "z", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_8 = {"slider3_valueChanged", 1, param_slot_8 };
    static const QUParameter param_slot_9[] = {
	{ "z", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_9 = {"OA_selector_activated", 1, param_slot_9 };
    static const QUParameter param_slot_10[] = {
	{ "z", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_10 = {"sliderThrottle_valueChanged", 1, param_slot_10 };
    static const QUMethod slot_11 = {"Go_button_clicked", 0, 0 };
    static const QUMethod slot_12 = {"languageChange", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "DrawSpring()", &slot_0, QMetaData::Public },
	{ "timerEvent(QTimerEvent*)", &slot_1, QMetaData::Public },
	{ "Reset()", &slot_2, QMetaData::Public },
	{ "dial1_valueChanged(int)", &slot_3, QMetaData::Public },
	{ "slider1_valueChanged(int)", &slot_4, QMetaData::Public },
	{ "STOP_button_pressed()", &slot_5, QMetaData::Public },
	{ "initialize_button_pressed()", &slot_6, QMetaData::Public },
	{ "OA_selector_textChanged(const QString&)", &slot_7, QMetaData::Public },
	{ "slider3_valueChanged(int)", &slot_8, QMetaData::Public },
	{ "OA_selector_activated(int)", &slot_9, QMetaData::Public },
	{ "sliderThrottle_valueChanged(int)", &slot_10, QMetaData::Public },
	{ "Go_button_clicked()", &slot_11, QMetaData::Public },
	{ "languageChange()", &slot_12, QMetaData::Protected }
    };
    metaObj = QMetaObject::new_metaobject(
	"spring01", parentObject,
	slot_tbl, 13,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_spring01.setMetaObject( metaObj );
    return metaObj;
}

void* spring01::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "spring01" ) )
	return this;
    return QDialog::qt_cast( clname );
}

bool spring01::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: DrawSpring(); break;
    case 1: timerEvent((QTimerEvent*)static_QUType_ptr.get(_o+1)); break;
    case 2: Reset(); break;
    case 3: dial1_valueChanged((int)static_QUType_int.get(_o+1)); break;
    case 4: slider1_valueChanged((int)static_QUType_int.get(_o+1)); break;
    case 5: STOP_button_pressed(); break;
    case 6: initialize_button_pressed(); break;
    case 7: OA_selector_textChanged((const QString&)static_QUType_QString.get(_o+1)); break;
    case 8: slider3_valueChanged((int)static_QUType_int.get(_o+1)); break;
    case 9: OA_selector_activated((int)static_QUType_int.get(_o+1)); break;
    case 10: sliderThrottle_valueChanged((int)static_QUType_int.get(_o+1)); break;
    case 11: Go_button_clicked(); break;
    case 12: languageChange(); break;
    default:
	return QDialog::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool spring01::qt_emit( int _id, QUObject* _o )
{
    return QDialog::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool spring01::qt_property( int id, int f, QVariant* v)
{
    return QDialog::qt_property( id, f, v);
}

bool spring01::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
