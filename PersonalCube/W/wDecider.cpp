#include <qcolordialog.h>
//#include <GL/gl.
//#include <unistd>     //Not sure what this is
#include <fstream>
#include <string>
#include <iostream>
//#include <stdio.h>
//#include <stdlib.h>
#include <qdatetime.h>

#include "wDecider.h"
#include "swissranger.h" //Ranger Library
//#include "awesimo.h"  //My values and functions

wDecider::wDecider(void)
{
//Initializer
//Perhaps decidce what to looke for?  set prioroties?  
	steeringSetting= 50;
	throttleSetting =33;
	//headSetting =47;
//normalTimer=startTimer(100);
pastHead = 0;
headDif=0;
headSetting = 50;
headOAtell = 0;
rangeDelta=0;
featureRangeBestPast=0;
featureRangeBest=0;
deltaTolerance=0;
initializeRangeLimits();
}

//
// Handles timer events for the digital clock widget.
// There are two different timers; one timer for updating the clock
// and another one for switching back from date mode to time mode.
//
//This should be moved "upward" to the calling object so the same timer can be used to 
void wDecider::decide(void)
    {

	featureSearch();

	if(selector ==0){
	//Basic OA
	    }else if(selector==1){
	//Best Ever OA
	    }else if(selector==2){
	//Demo
	    }else if(selector==3){
	//FollowingF
	decideTrack();
	    }else if(selector==4){
		//FollowH
	decideHeadPID();
    	}	else{
	decideOA();}
}	
//awesimo.head_set(awesimo.dev.fd,z*25);  


//b'
void wDecider::decideOA(void)
{
//steeringSetting=steeringSetting + 1;
	featureSearch();
	if (featureStatus !=1)
    {//Checks if there is a staturated pixel(s) in view
        //obstacleSearchOA();
        headOA();
        throttleSetting = 40;
	}//Avoid obstacles and search
	else
    {
        double dummy;
        dummy= headRange2AwesimoRange();
        steeringSetting=steerFix(int(dummy*.625));
        decideTrackFeature();
        decideHeadPID();
	}	
//PastSteeringSetting=steeringSetting;
	//PastThrottleSetting=throttleSetting;	
	//steeringSetting=featureX*5/8;
	//steeringSetting=int(featureBearingX*5/8);

}

void	wDecider::obstacleSearchOA(void)
{
//OA for a movable head.  Need to integrate 
int proxField[16];
//int throttleField[20];
//int steeringField[20];
int seenField[4];
//proxField[2]=FixHead2BodyOAField();
//Now have field of values to make decisions on
if(headSetting==LOOK_VERY_FAR_LEFT)
{ 
seenField[0]= 0;
seenField[1]= 1;
seenField[2]= 2;
seenField[3]= 3;
} else if(headSetting==LOOK_FAR_LEFT) { 
seenField[0]= 2;
seenField[1]= 3;
seenField[2]= 4;
seenField[3]= 5;
} 
else if(headSetting==LOOK_LEFT) { 
seenField[0]= 4;
seenField[1]= 5;
seenField[2]= 6;
seenField[3]= 7;
} 
else if(headSetting==LOOK_AHEAD) { 
seenField[0]= 6;
seenField[1]= 7;
seenField[2]= 8;
seenField[3]= 9;
} 
else if(headSetting==LOOK_RIGHT) { 
seenField[0]= 8;
seenField[1]= 9;
seenField[2]= 10;
seenField[3]= 11;
}
else if(headSetting==LOOK_FAR_RIGHT) { 
seenField[0]= 10;
seenField[1]= 11;
seenField[2]= 12;
seenField[3]= 13;
}
else if(headSetting==LOOK_VERY_FAR_RIGHT) { 
seenField[0]= 12;
seenField[1]= 13;
seenField[2]= 14;
seenField[3]= 15;
}else{//Danger!!!!
	throttleSetting=35;	//BRAKE
	}

proxField[seenField[0]]=OAFieldEval(0,seenField[0]);
proxField[seenField[1]]=OAFieldEval(1,seenField[1]);
proxField[seenField[2]]=OAFieldEval(2,seenField[2]);
proxField[seenField[3]]=OAFieldEval(3,seenField[3]);

//Now we have a field of values;
//OaFieldCompare(proxField);

//return proxField[2];
}	

void wDecider::OaFieldCompare(int f[16])
{
int left;
int right;
//int ahead;
left = (f[0]+ f[1]+ f[2]+ f[3]+ f[4]+ f[5]+ f[6]+ f[7])/8;
right = (f[8]+ f[9]+ f[10]+ f[11]+ f[12]+ f[13]+ f[14]+ f[15])/8;

if(left<right)
	{steeringSetting= 30;}
	else
	{steeringSetting= 70;}
}

//Called from obstacleSearch(), splits front 180degreed into 20 pices and returns a value for objects in the corresponding range.  
//int wDecider::FixHead2BodyOAField(int *proxFieldPtr)
int wDecider::FixHead2BodyOAField(void)

{
//int proxField[16];
int seenField[4];

if(headSetting==LOOK_VERY_FAR_LEFT)
{ 
seenField[0]= 0;
seenField[1]= 1;
seenField[2]= 2;
seenField[3]= 3;
} else if(headSetting==LOOK_FAR_LEFT) { 
seenField[0]= 2;
seenField[1]= 3;
seenField[2]= 4;
seenField[3]= 5;
} 
else if(headSetting==LOOK_LEFT) { 
seenField[0]= 4;
seenField[1]= 5;
seenField[2]= 6;
seenField[3]= 7;
} 
else if(headSetting==LOOK_AHEAD) { 
seenField[0]= 6;
seenField[1]= 7;
seenField[2]= 8;
seenField[3]= 9;
} 
else if(headSetting==LOOK_RIGHT) { 
seenField[0]= 8;
seenField[1]= 9;
seenField[2]= 10;
seenField[3]= 11;
}
else if(headSetting==LOOK_FAR_RIGHT) { 
seenField[0]= 10;
seenField[1]= 11;
seenField[2]= 12;
seenField[3]= 13;
}
else if(headSetting==LOOK_VERY_FAR_RIGHT) { 
seenField[0]= 12;
seenField[1]= 13;
seenField[2]= 14;
seenField[3]= 15;
}

//*(proxFieldPtr+seenField[0]=OAFieldEval(0,seenField[0]);
//*(proxFieldPtr+seenField[1]=OAFieldEval(1,seenField[1]);
//*(proxFieldPtr+seenField[2]]=OAFieldEval(2,seenField[2]);
//*(proxFieldPtr+seenField[3]]=OAFieldEval(3,seenField[3]);


return 1;
}

//called from FixHead2BodyOAField
//returns value of segment of field of view
//Could be inteligent or relative.  Trying relative
int wDecider::OAFieldEval(int field,int direction)
{
int startPix[4]={0, 40, 80, 120};
int fieldVal;
fieldVal= 0;

for (int i = startPix[field]; i < startPix[field]+40; i++)
	{
	int k; 
	k= 160*i;
	for (int j = 0; j <123; j++)
		{
			if (*(pixPtr+k+j+19840)>noiseFilter)
				{ 
				if(*(pixPtr+k+j)<yellow[direction])
					{	fieldVal=  fieldVal+1;}
				else if(*(pixPtr+k+j)<red[direction])
					{	fieldVal=  fieldVal+8;}
				}
		//fieldVal=  fieldVal+*(pixPtr+k+j);
		}
//fieldVal = int(fieldVal/124);
} 

return fieldVal;
}



//Does not send an invalid number to the steering setting.  
int wDecider::steerFix(int steer)
{	int fixedSteer;
	if (steer<0)
	{fixedSteer=0;}
	else if(steer>100){
	fixedSteer =100;}
	else{fixedSteer=steer;}
return fixedSteer;
}

//Rough because this is pixel to pixel, skipping a higher level angle.  
double wDecider::headRange2AwesimoRange(void)
	{
	double newBearing;
	double headOffCenter;
	headOffCenter=50-headSetting;
	newBearing =50-5*headOffCenter+featureBearingX*.5; 
	return newBearing;
	}

//This is the decision maker when there is an object to be tracked.  
//It is called from OAdecide(void) and controlls throttle and steering settings
void wDecider::decideTrackFeature(void)
{
//Make decisions on throttle and steering when the tracked feature is visible.  
//info: range and delta(closing speed) from multiple range measurements
featureGoalRange=65;
featureTolerance= 8;
deltaTolerance=3;
int correctRange;
int correctDelta;
int x1,x2,x3,x4,x5,x6;
int s1,s2,s3,s4,s5,s6,s7,s8;

rangeDelta=featureRangeBest-featureRangeBestPast;
correctRange=RangeCorrection(featureRangeBest);
correctDelta=  correctRange - RangeCorrection(featureRangeBestPast);
//x1=x2=x3=x4=x5=x6=1;
//s1=s2=s3=s4=s5=s6=s7=s8=1;
x1= ((correctRange>featureGoalRange-featureTolerance) && (correctRange<featureGoalRange+featureTolerance));//in band; 
x2= ((correctDelta < deltaTolerance) && (correctDelta > -1*deltaTolerance));  //in tolleracne delta
x3= (correctDelta > deltaTolerance);// moving away
x4= (correctDelta < -1*deltaTolerance);// moving towards
x5= (correctRange<featureGoalRange-featureTolerance);//Too close 
x6= (correctRange>featureGoalRange+featureTolerance);//Too Far

s1=(x1 && x2);
s2=(x6 && x3);
s3=(x6 && x2);
s4=(x6 && x4);
s5=(x5 && x4);
s6 =(x5 && x2);
s7 =(x5 && x3);
s8 = (x1 && x4);

	if(s1){  //in both tolerances
	//keep throttle set
	printf("S1 In Bands \n");
	SetThrottle(0);
	} else if(s2){
	printf("S2 speed up->too far and movine away \n");
	SetThrottle(1);
	} else if(s3){
	printf("S3 speed up->to far and holding stable \n");
	SetThrottle(1);///////////////////////////0
	} else if(s4){
	printf("S4 keep throttle set-> too far and moving towards \n");
	SetThrottle(0);////////////////////////////0
	} else if(s5){ 
	printf("S5 slow down fast->too close and moving towards \n");
	SetThrottle(-5);
	} else if(s6){
	printf("S6 slow down/back1 -> too close and holding stable \n ");
	SetThrottle(-1);
	} else if(s7){
	printf("S7 keep throttle set.-> too lcose amd moving away \n");
	SetThrottle(0);
	} else if(s8){
	printf("S8 keep throttle set--in band and moving towards. \n ");
	SetThrottle(-1);
	} else {
	printf("S9 in band and moving away. It happens\n"); 
	SetThrottle(1);

//throttleSetting = 43;
	}

}

//Timed stuff that is activated by a timer in the calling program
//void Rangergl::MytimerEvent( QTimerEvent *e )
void wDecider::decideTrack(void)
{
//steeringSetting=steeringSetting + 1;
	featureSearch();

	PastSteeringSetting=steeringSetting;
	PastThrottleSetting=throttleSetting;	
	//steeringSetting=featureX*5/8;
	steeringSetting=int(featureBearingX*5/8);

}

void wDecider::headOA(void)
{
PastHeadSetting=headSetting;
if(headOAtell>25)
{
	headSetting = LOOK_LEFT;
	}else if(headOAtell>15)
	{
	headSetting = LOOK_AHEAD;
	
	}else if(headOAtell>10)
	{
	headSetting = LOOK_RIGHT;
	}else {
	headSetting = LOOK_AHEAD;
	}


if(headOAtell > 30)
	{headOAtell=0;}
headOAtell++;

}


void wDecider::decideHead(void)
{
//steeringSetting=steeringSetting + 1;
int step;
double headDif;
	featureSearch();

	PastSteeringSetting=steeringSetting;
	PastThrottleSetting=throttleSetting;	
	PastHeadSetting=headSetting;
 headDif=80-featureBearingX;	
// headDif=50-5/8*featureBearingX;

if(abs(int(headDif))<10)
	{
	step = 0;
	} else if (headDif > 0 && headDif <30)
 	{
	step = 1;
	} else if (headDif < 0 && headDif >30)
	{step= -1;
	} else if (headDif >30)
	{step = 3;}
	else
	{ step = -3;}
	
	
	headSetting =headSetting+step;
	//steeringSetting=featureX*5/8;
	//steeringSetting=int(featureBearingX*5/8);
//run decision process here.  
}


//Misnomer.  Not PID but smarter than regular head tracking.
void wDecider::decideHeadPID(void)
{
//steeringSetting=steeringSetting + 1;
//int step;
//double headDif;
//int delta;
pastHead=headSetting;
	featureSearch();
//delta = steeringSetting-PastSteeringSetting;

//	PastSteeringSetting=steeringSetting;
//	PastThrottleSetting=throttleSetting;	
//	PastHeadSetting=headSetting;
 headDif=int(featureBearingX-80);	
// headDif=50-5/8*featureBearingX;
if(abs(headDif)>25)
{
headSetting = int(headSetting+headDif*.08);

//headSetting = int(headSetting+headDif*.12 -.04*headDif);
}else {};	
headDif = headSetting-pastHead;

//	headSetting =headSetting+step;
	//steeringSetting=featureX*5/8;
	//steeringSetting=int(featureBearingX*5/8);
//run decision process here.  
}

/*uses bit shifting to remove 8th bit from unsigned short number
Also removes empty 1st and 2nd bits.
*/
short unsigned int wDecider::pinchNum(short unsigned int number) {
	/*short unsigned int lowNum;
	short unsigned int hiNum;
	lowNum=number;//1111111X111111XX
	hiNum=number;//1111111X11111100
	lowNum <<= 8;//X|111111XX|00000000>x gone
	lowNum >>= 10;//0000000000111111|XX->XXgone
//	lowNum >>= 16;//0000000000111111|XX->XXgone

	hiNum= hiNum >> 9;//0000000001111111|X->x gone
	hiNum= hiNum << 6;//001111111000000
	number =hiNum+lowNum;//0001111111111111
*/	
	number =number >>2;
	//number =number <<4;

	return number;
}


/*finds reflector in field of view
Uses and changes 
	double featureRange;
	double featureBearingX;
	double featureBearingY;
	double featureStatus;
*/
void wDecider::featureSearch()
{

int k;
//featureRange = 5;
featureStatus=0;

  for (int i = 0; i < 160 && featureStatus==0 ; ++i) {
        for (int j = 0; j < 124 && featureStatus==0 ; ++j) {

		k=j+124*i;
	
	//sets satruated pixels to light blue and place them before the scene.
		if (saturationCheck(k))
		{
		trackSaturationFeature(k);
		featureBearingX=i;
		featureBearingY=j;
	 	featureStatus=1;
        	}//end 1-124 loop
	
	}
if(i >=159){featureStatus=0;}
}

//if(i >=160 && j >=124)
//featureBearingXPast=featureBearingX;//double(i)*(p/160);
//featureBearingYPast=featureBearingY;// 
//featureRangePast = featureRange;
featureRangeBestPast=featureRangeBest;
featureRangeBest= int(wuzzyFix(featureRangeBest, 3,pinchNum(featureRange),1));

}

//if(i >=160 && j >=124)
//featureRangeBest= int(wuzzyFix(featureRangeBest, 3,pinchNum(featureRange),1));


/*Do a "wall following " of the saturated patch and record path
*/
void wDecider::trackSaturationFeature(int k)
{
int startPoint = k;
//int x,y;
//int pos;
//double bestBearingX;
//double bestBearingY;
unsigned short int bestRange;
int dirs[8];
unsigned short int dirCount;

dirCount = 1;

dirs[0]=-125;
dirs[1]=-124;
dirs[2]= -123;
dirs[3]= 1;
dirs[4]= 125;
dirs[5]= 124;
dirs[6]= 123;
dirs[7]= -1;
//pos = dirs[dirCount];
dirCount++;
//bestRange=0;

//range=pixels[pos];
//intense=pixels[pos+19840];

//*(pixels+80);
//bestRange=*(pixPtr + startPoint+dirs[1]+dirs[1]);
//bestRange= pixels[startPoint+dirs[1]+dirs[1]];
bestRange= *(pixPtr+startPoint+dirs[1]);

featureRange=bestRange; //int(wuzzyFix(featureRange, 6, bestRange,1));
//bestRange;

//featureBearingX=int(bestBearingX);
//featureBearingY=int(bestBearingY);
}

int wDecider::saturationCheck(int k)
	{
	int sat=0; 
	if (*(pixPtr + k+19840)==65532)
		{sat=1;}
	return sat;
}


int wDecider::getfeatureBearingY(void)
{
return int(featureBearingY);
}

double wDecider::wuzzyFix(unsigned short best, double bestStatus, unsigned short newData, double newDataStatus)
{
double wuzzy,worth;
double w1,w2;

w1=best*bestStatus;
w2=newData*newDataStatus;
worth = bestStatus+newDataStatus;
//worth= newDataStatus/bestStatus;
//wuzzy=best-(best-newData)*worth;
wuzzy = (w1+w2)/worth;
return wuzzy;
}

void wDecider::initializeRangeLimits(void)
	{
	yellow[0]=70;
	red[0]=35;
	yellow[1]=70;
	red[1]=35;
	yellow[2]=70;
	red[2]=35;
	yellow[3]=100;
	red[3]=50;
	yellow[4]=120;
	red[4]=60;
	yellow[5]=140;
	red[5]=70;
	yellow[6]=160;
	red[6]=80;
	yellow[7]=160;
	red[7]=90;
	yellow[8]=160;
	red[8]=90;
	yellow[9]=180;
	red[9]=100;
	yellow[10]=200;
	red[10]=140;
	yellow[11]=220;
	red[11]=160;
	yellow[12]=400;
	red[12]=400;
	yellow[13]=400;
	red[13]=400;
	yellow[14]=400;
	red[14]=400;
	yellow[15]=400;
	red[15]=400;
}

int wDecider::RangeCorrection(int rawRange)
{
int correctedRange;

correctedRange =.04335*rawRange-160;
return int(correctedRange);
}

void wDecider:: SetThrottle(int dir)
{

//goal, skip over the dead zone
//int lowBrake, highBrake;
int goal;
//lowBrake = 30;
//highBrake = 43;
goal = dir + throttleSetting ;
/*
int X1, X2, X3;
X1= (goal> 30);
X2= (goal <43);

X3= (X1 && X2);
if (!X3)
{throttleSetting = dir + throttleSetting ;}
else if(X1)
	{throttleSetting = 30+dir ;}
else if(X2)
	{throttleSetting = 44+dir ;}
*/
if (goal > 51){goal = 51;}
if (goal < 23){goal = 23;}
if (goal > 29 && goal <43) { 
	
	if (dir > 0) {goal =44;} 
	else if (dir <= 0){goal = 28;}
	
	}

throttleSetting = goal;
}
