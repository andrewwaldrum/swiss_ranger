#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <signal.h>
#include <math.h>
#include <sys/time.h>

#include "serial-io.h"
#include "serial-main.h"

SERIAL_DEVICE            device;

void 
commShutdown( int sig ) {
  close(device.fd);
  exit(1);
}

void
setSerialDevice( SERIAL_DEVICE *dev )
{
  dev->baud           = DEFAULT_SERIAL_BAUD;
  dev->parity         = DEFAULT_SERIAL_PARITY;
  dev->fd             = -1;
  dev->databits       = DEFAULT_SERIAL_DATA_BITS;
  dev->stopbits       = DEFAULT_SERIAL_STOP_BITS;
  dev->hwf            = DEFAULT_SERIAL_HWF;
  dev->swf            = DEFAULT_SERIAL_SWF;
  dev->ttyport        =
    (char *) malloc( (strlen(DEFAULT_SERIAL_PORT)+1)*sizeof(char) );
  strcpy( dev->ttyport, DEFAULT_SERIAL_PORT );
}

void
startSerialDevice( SERIAL_DEVICE *dev )
{
  setSerialDevice( dev );
  fprintf( stderr, "INFO: connect TTY %-14s ...... ", dev->ttyport );
  DEVICE_connect_port( dev );
  if( dev->fd == -1) {
    fprintf( stderr, "failed\n" );
    exit(1);
  }
  fprintf( stderr, "ok\n" );
  fprintf( stderr, "INFO: set port param %6d:%d%c%d ....... ",
	   dev->baud,
	   dev->databits,
	   ( dev->parity==N ?
	     'N' : dev->parity==E ? 'E' : 'O' ),
	   dev->stopbits );
  DEVICE_set_params( *dev );
  fprintf( stderr, "ok\n" );
}

void
print_usage( void )
{
  fprintf( stderr, "usage: laser-server [-ini <INI-FILE>] [-dev <DEVICE>] [-pls] [-lms]\n" );
}

int 
main( void ) {

  startSerialDevice( &device );

  signal(SIGINT,commShutdown);

  while(TRUE) {
    
    usleep( 10000 );

  } /* while (TRUE) */

}
